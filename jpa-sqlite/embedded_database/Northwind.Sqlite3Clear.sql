-- Valon Hoti @ 2010-07-04 [YYYY-MM-DD]
-- Prishtine,10000
-- KOSOVE
-- DATABASE : Northwind 
-- ORIGIN   : MS SQL 
-- SOURCE   : SQLITE 3
-- 
-- Converted TABLES from MS SQL 2000  to Sqlite 3 
-- 
-- Categories
DROP TABLE IF EXISTS [Categories];
CREATE TABLE [Categories]
(      [CategoryID] INTEGER PRIMARY KEY AUTOINCREMENT,
       [CategoryName] TEXT,
       [Description] TEXT,
       [Picture] BLOB
);

--
SELECT * FROM [Categories];
-- CustomerCustomerDemo
DROP TABLE IF EXISTS [CustomerCustomerDemo];
CREATE TABLE [CustomerCustomerDemo](
   [CustomerID]TEXT NOT NULL,
   [CustomerTypeID]TEXT NOT NULL,
   PRIMARY KEY ("CustomerID","CustomerTypeID")
);
SELECT * FROM [CustomerCustomerDemo];
-- CustomerDemographics
DROP TABLE IF EXISTS [CustomerDemographics];
CREATE TABLE [CustomerDemographics](
   [CustomerTypeID]TEXT NOT NULL,
   [CustomerDesc]TEXT,
    PRIMARY KEY ("CustomerTypeID")
);
SELECT * FROM [CustomerCustomerDemo];
-- Customers
DROP TABLE IF EXISTS [Customers];
CREATE TABLE [Customers]
(      [CustomerID] TEXT,
       [CompanyName] TEXT,
       [ContactName] TEXT,
       [ContactTitle] TEXT,
       [Address] TEXT,
       [City] TEXT,
       [Region] TEXT,
       [PostalCode] TEXT,
       [Country] TEXT,
       [Phone] TEXT,
       [Fax] TEXT,
       PRIMARY KEY (`CustomerID`)
);
SELECT * FROM [Customers];
-- Employee
DROP TABLE IF EXISTS [Employees];
--
CREATE TABLE [Employees]
(      [EmployeeID] INTEGER PRIMARY KEY AUTOINCREMENT,
       [LastName] TEXT,
       [FirstName] TEXT,
       [Title] TEXT,
       [TitleOfCourtesy] TEXT,
       [BirthDate] DATE,
       [HireDate] DATE,
       [Address] TEXT,
       [City] TEXT,
       [Region] TEXT,
       [PostalCode] TEXT,
       [Country] TEXT,
       [HomePhone] TEXT,
       [Extension] TEXT,
       [Photo] BLOB,
       [Notes] TEXT,
       [ReportsTo] INTEGER,
       [PhotoPath] TEXT
);
--
-- EmployeeTerritories
DROP TABLE IF EXISTS [EmployeeTerritories];
CREATE TABLE [EmployeeTerritories](
   [EmployeeID]INTEGER NOT NULL,
   [TerritoryID]TEXT NOT NULL,
    PRIMARY KEY ("EmployeeID","TerritoryID")
);
SELECT * FROM [EmployeeTerritories];
--[Order Details]
DROP TABLE IF EXISTS[Order_Details];
CREATE TABLE [Order_Details](
   [ODID] INTEGER PRIMARY KEY AUTOINCREMENT,
   [OrderID]INTEGER NOT NULL,
   [ProductID]INTEGER NOT NULL,
   [UnitPrice]FLOAT NOT NULL DEFAULT 0,
   [Quantity]INTEGER NOT NULL DEFAULT 1,
   [Discount]FLOAT NOT NULL DEFAULT 0,
    CHECK ([Discount]>=(0) AND [Discount]<=(1)),
    CHECK ([Quantity]>(0)),
    CHECK ([UnitPrice]>=(0))
);
CREATE INDEX [OrderID]ON [Order_Details]("OrderID" ASC);
CREATE INDEX [ProductID]ON [Order_Details]("ProductID" ASC);	

SELECT * FROM [Order_Details];
--Orders
--Orders
DROP TABLE IF EXISTS [Orders];
CREATE TABLE [Orders](
   [OrderID]INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   [CustomerID]TEXT,
   [EmployeeID]INTEGER,
   [OrderDate]DATE,
   [RequiredDate]DATE,
   [ShippedDate]DATE,
   [ShipVia]INTEGER,
   [Freight]FLOAT DEFAULT 0,
   [ShipName]TEXT,
   [ShipAddress]TEXT,
   [ShipCity]TEXT,
   [ShipRegion]TEXT,
   [ShipPostalCode]TEXT,
   [ShipCountry]TEXT,
   FOREIGN KEY(CustomerID) REFERENCES Customers(CustomerID)
);
CREATE INDEX [OrderDate]ON [Orders]("OrderDate" ASC);
CREATE INDEX [CustomerID]ON [Orders]("CustomerID" ASC);	
--CREATE INDEX [ShippedDate]ON [Orders]("ShippedDate" ASC);
--CREATE INDEX [ShipPostalCode]ON [Orders]("ShipPostalCode" ASC);

--
SELECT * FROM [Orders];
--Products
DROP TABLE IF EXISTS [Products];
CREATE TABLE [Products](
   [ProductID]INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   [ProductName]TEXT NOT NULL,
   [SupplierID]INTEGER,
   [CategoryID]INTEGER,
   [QuantityPerUnit]TEXT,
   [UnitPrice]FLOAT DEFAULT 0,
   [UnitsInStock]INTEGER DEFAULT 0,
   [UnitsOnOrder]INTEGER DEFAULT 0,
   [ReorderLevel]INTEGER DEFAULT 0,
   [Discontinued]TEXT NOT NULL DEFAULT '0',
    CHECK ([UnitPrice]>=(0)),
    CHECK ([ReorderLevel]>=(0)),
    CHECK ([UnitsInStock]>=(0)),
    CHECK ([UnitsOnOrder]>=(0))
);

--CREATE INDEX [ProductName]ON [Products]("ProductName" ASC);

SELECT * FROM [Products];
--Regions
DROP TABLE IF EXISTS [Regions];
CREATE TABLE [Regions](
   [RegionID]INTEGER NOT NULL PRIMARY KEY,
   [RegionDescription]TEXT NOT NULL
);
SELECT * FROM [Regions];
--Shippers
DROP TABLE IF EXISTS[Shippers];
CREATE TABLE [Shippers](
   [ShipperID]INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   [CompanyName]TEXT NOT NULL,
   [Phone]TEXT
);
SELECT * FROM [Shippers];
--Suppliers
DROP TABLE IF EXISTS [Suppliers];
CREATE TABLE [Suppliers](
   [SupplierID]INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   [CompanyName]TEXT NOT NULL,
   [ContactName]TEXT,
   [ContactTitle]TEXT,
   [Address]TEXT,
   [City]TEXT,
   [Region]TEXT,
   [PostalCode]TEXT,
   [Country]TEXT,
   [Phone]TEXT,
   [Fax]TEXT,
   [HomePage]TEXT
);

SELECT * FROM [Suppliers];
--Territories
DROP TABLE IF EXISTS[Territories];
CREATE TABLE [Territories](
   [TerritoryID]TEXT NOT NULL,
   [TerritoryDescription]TEXT NOT NULL,
   [RegionID]INTEGER NOT NULL,
    PRIMARY KEY ("TerritoryID")
);
SELECT * FROM [Territories];