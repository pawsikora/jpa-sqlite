package model.northwind;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;


/**
 * The persistent class for the "Categories" database table.
 * 
 */
@Entity
@Table(name="\"Categories\"")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="\"CategoryID\"")
	private int categoryID;

	@Column(name="\"CategoryName\"")
	private String categoryName;

	@Column(name="\"Description\"")
	private String description;

	@Lob
	@Column(name="\"Picture\"")
	private Object picture;

	//@OneToMany(mappedBy="category")
	//private Collection<Product> products;
	
	public Category() {
	}
	
//	public Collection<Product> getProducts() {
//		return products;
//	}
//	
//	public void setProducts(Collection<Product> products) {
//		this.products = products;
//	}

	public Category(String categoryName, String description) {
		super();
		this.categoryName = categoryName;
		this.description = description;
	}

	public int getCategoryID() {
		return this.categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getPicture() {
		return this.picture;
	}

	public void setPicture(Object picture) {
		this.picture = picture;
	}

}