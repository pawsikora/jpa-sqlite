package model.northwind;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


/**
 * The persistent class for the "Employees" database table.
 * 
 */
@Entity
@Table(name="\"Employees\"")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="\"EmployeeID\"")
	private int employeeID;
	
	@Column(name="\"Address\"")
	private String address;

	@Column(name="\"BirthDate\"")
	private Date birthDate;

	@Column(name="\"City\"")
	private String city;

	@Column(name="\"Country\"")
	private String country;

	@Column(name="\"Extension\"")
	private String extension;

	@Column(name="\"FirstName\"")
	private String firstName;

	@Column(name="\"HireDate\"")
	private Date hireDate;

	@Column(name="\"HomePhone\"")
	private String homePhone;

	@Column(name="\"LastName\"")
	private String lastName;

	@Column(name="\"Notes\"")
	private String notes;

	@Lob
	@Column(name="\"Photo\"")
	private Object photo;

	@Column(name="\"PhotoPath\"")
	private String photoPath;

	@Column(name="\"PostalCode\"")
	private String postalCode;

	@Column(name="\"Region\"")
	private String region;

	@Column(name="\"ReportsTo\"")
	private int reportsTo;

	@Column(name="\"Title\"")
	private String title;

	@Column(name="\"TitleOfCourtesy\"")
	private String titleOfCourtesy;

	public Employee() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public Employee(String address, Date birthDate, String city,
			String country, String extension, String firstName, Date hireDate,
			String homePhone, String lastName, String notes, String postalCode,
			int reportsTo, String title, String titleOfCourtesy) {
		super();
		this.address = address;
		this.birthDate = birthDate;
		this.city = city;
		this.country = country;
		this.extension = extension;
		this.firstName = firstName;
		this.hireDate = hireDate;
		this.homePhone = homePhone;
		this.lastName = lastName;
		this.notes = notes;
		this.postalCode = postalCode;
		this.reportsTo = reportsTo;
		this.title = title;
		this.titleOfCourtesy = titleOfCourtesy;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getEmployeeID() {
		return this.employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getExtension() {
		return this.extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public String getHomePhone() {
		return this.homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Object getPhoto() {
		return this.photo;
	}

	public void setPhoto(Object photo) {
		this.photo = photo;
	}

	public String getPhotoPath() {
		return this.photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public int getReportsTo() {
		return this.reportsTo;
	}

	public void setReportsTo(int reportsTo) {
		this.reportsTo = reportsTo;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleOfCourtesy() {
		return this.titleOfCourtesy;
	}

	public void setTitleOfCourtesy(String titleOfCourtesy) {
		this.titleOfCourtesy = titleOfCourtesy;
	}

}