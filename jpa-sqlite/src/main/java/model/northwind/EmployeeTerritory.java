package model.northwind;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "EmployeeTerritories" database table.
 * 
 */
@Entity
@IdClass(EmployeeTerritoryId.class)
@Table(name="\"EmployeeTerritories\"")
public class EmployeeTerritory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"EmployeeID\"")
	private int employeeID;

	@Id
	@Column(name="\"TerritoryID\"")
	private String territoryID;

	public EmployeeTerritory() {
	}

	public int getEmployeeID() {
		return this.employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getTerritoryID() {
		return this.territoryID;
	}

	public void setTerritoryID(String territoryID) {
		this.territoryID = territoryID;
	}

}