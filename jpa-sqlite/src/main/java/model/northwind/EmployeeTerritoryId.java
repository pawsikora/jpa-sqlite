package model.northwind;

import java.io.Serializable;

public class EmployeeTerritoryId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int employeeID;
	String territoryID;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + employeeID;
		result = prime * result
				+ ((territoryID == null) ? 0 : territoryID.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeTerritoryId other = (EmployeeTerritoryId) obj;
		if (employeeID != other.employeeID)
			return false;
		if (territoryID == null) {
			if (other.territoryID != null)
				return false;
		} else if (!territoryID.equals(other.territoryID))
			return false;
		return true;
	}
}

