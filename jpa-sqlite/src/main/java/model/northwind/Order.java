package model.northwind;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the "Orders" database table.
 * 
 */
@Entity
@Table(name="\"Orders\"")
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="\"OrderID\"")
	private int orderID;

	@ManyToOne(optional = false)
	@JoinColumn(name="\"CustomerID\"")
	private Customer customer;

	@Column(name="\"EmployeeID\"")
	private int employeeID;

	@Column(name="\"Freight\"")
	private Float freight;

	@Temporal(TemporalType.DATE)
	@Column(name="\"OrderDate\"")
	private Date orderDate;

	@Temporal(TemporalType.DATE)
	@Column(name="\"RequiredDate\"")
	private Date requiredDate;

	@Column(name="\"ShipAddress\"")
	private String shipAddress;

	@Column(name="\"ShipCity\"")
	private String shipCity;

	public Order(int orderID, Customer customer, int employeeID,
			Float freight, Date orderDate, Date requiredDate,
			String shipAddress, String shipCity, String shipCountry,
			String shipName, Date shippedDate, Shipper shipVia) {
		super();
		this.orderID = orderID;
		this.customer = customer;
		this.employeeID = employeeID;
		this.freight = freight;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shipAddress = shipAddress;
		this.shipCity = shipCity;
		this.shipCountry = shipCountry;
		this.shipName = shipName;
		this.shippedDate = shippedDate;
		this.shipVia = shipVia;
	}

	@Column(name="\"ShipCountry\"")
	private String shipCountry;

	@Column(name="\"ShipName\"")
	private String shipName;

	@Temporal(TemporalType.DATE)
	@Column(name="\"ShippedDate\"")
	private Date shippedDate;

	@Column(name="\"ShipPostalCode\"")
	private String shipPostalCode;

	@Column(name="\"ShipRegion\"")
	private String shipRegion;

	@ManyToOne(optional=false)
	@JoinColumn(name="\"ShipVia\"")
	private Shipper shipVia;

	public Order() {
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getEmployeeID() {
		return this.employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public Float getFreight() {
		return this.freight;
	}

	public void setFreight(Float freight) {
		this.freight = freight;
	}

	public Date getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public int getOrderID() {
		return this.orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public Date getRequiredDate() {
		return this.requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getShipAddress() {
		return this.shipAddress;
	}

	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}

	public String getShipCity() {
		return this.shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}

	public String getShipCountry() {
		return this.shipCountry;
	}

	public void setShipCountry(String shipCountry) {
		this.shipCountry = shipCountry;
	}

	public String getShipName() {
		return this.shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public Date getShippedDate() {
		return this.shippedDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public String getShipPostalCode() {
		return this.shipPostalCode;
	}

	public void setShipPostalCode(String shipPostalCode) {
		this.shipPostalCode = shipPostalCode;
	}

	public String getShipRegion() {
		return this.shipRegion;
	}

	public void setShipRegion(String shipRegion) {
		this.shipRegion = shipRegion;
	}

	public Shipper getShipVia() {
		return this.shipVia;
	}

	public void setShipVia(Shipper shipVia) {
		this.shipVia = shipVia;
	}

}