package model.northwind;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

/**
 * The persistent class for the "Order Details" database table.
 * 
 */
@Entity
@Table(name = "\"Order_Details\"")
public class OrderDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "\"ODID\"")
	private int odID;
	
	public int getOdID() {
		return odID;
	}

	public void setOdID(int odID) {
		this.odID = odID;
	}

	@ManyToOne(optional=false)
	@JoinColumn(name="\"OrderID\"")
	private Order order;

//
	@ManyToOne(optional=false)
	@JoinColumn(name="\"ProductID\"")
	private Product product;
	
	public OrderDetail(Order order, Product product, Float discount, int quantity,
			Float unitPrice) {
		super();
		this.order = order;
		this.product = product;
		this.discount = discount;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		
	}
	
	@Column(name = "\"Discount\"")
	private Float discount;
	
	@Column(name = "\"Quantity\"")
	private int quantity;

	@Column(name = "\"UnitPrice\"")
	private Float unitPrice;

//	@ManyToOne(optional=false)
//	@JoinColumn(name = "\"ProductID\"")
//	private Product product;
	
	
	public OrderDetail() {
	}

	public Float getDiscount() {
		return this.discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Float getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Product getProductID() {
		return this.product;
	}

	public void setProductID(Product product) {
		this.product = product;
	}

}