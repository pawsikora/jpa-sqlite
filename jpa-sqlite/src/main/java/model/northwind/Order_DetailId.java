package model.northwind;

import java.io.Serializable;

public class Order_DetailId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int productID;
	int orderID;
	
	public Order_DetailId() {
		// TODO Auto-generated constructor stub
	}
	
	public Order_DetailId(int productID, int orderID) {
		super();
		this.productID = productID;
		this.orderID = orderID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + orderID;
		result = prime * result + productID;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order_DetailId other = (Order_DetailId) obj;
		if (orderID != other.orderID)
			return false;
		if (productID != other.productID)
			return false;
		return true;
	}

}
