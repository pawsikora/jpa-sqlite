package model.northwind;

import java.io.Serializable;
import java.math.BigDecimal;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the "Products" database table.
 * 
 */
@Entity
@Table(name="\"Products\"")
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="\"CategoryID\"")
	private int categoryID;

	@Column(name="\"Discontinued\"")
	private String discontinued;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="\"ProductID\"")
	private int productID;

	@Column(name="\"ProductName\"")
	private String productName;

	@Column(name="\"QuantityPerUnit\"")
	private String quantityPerUnit;

	@Column(name="\"ReorderLevel\"")
	private int reorderLevel;

	@ManyToOne(optional=false)
	@JoinColumn(name="\"SupplierID\"")
	private Supplier supplier;

	@Column(name="\"UnitPrice\"")
	private Float unitPrice;

	@Column(name="\"UnitsInStock\"")
	private int unitsInStock;

	@Column(name="\"UnitsOnOrder\"")
	private int unitsOnOrder;

	public Product() {
	}

	public int getCategoryID() {
		return this.categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getDiscontinued() {
		return this.discontinued;
	}

	public void setDiscontinued(String discontinued) {
		this.discontinued = discontinued;
	}

	public int getProductID() {
		return this.productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getQuantityPerUnit() {
		return this.quantityPerUnit;
	}

	public void setQuantityPerUnit(String quantityPerUnit) {
		this.quantityPerUnit = quantityPerUnit;
	}

	public int getReorderLevel() {
		return this.reorderLevel;
	}

	public void setReorderLevel(int reorderLevel) {
		this.reorderLevel = reorderLevel;
	}

	public Supplier getSupplier() {
		return this.supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Float getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getUnitsInStock() {
		return this.unitsInStock;
	}

	public void setUnitsInStock(int unitsInStock) {
		this.unitsInStock = unitsInStock;
	}

	public int getUnitsOnOrder() {
		return this.unitsOnOrder;
	}

	public void setUnitsOnOrder(int unitsOnOrder) {
		this.unitsOnOrder = unitsOnOrder;
	}

	public Product(int categoryID, String discontinued, String productName,
			String quantityPerUnit, int reorderLevel, Supplier supplier,
			Float unitPrice, int unitsInStock, int unitsOnOrder) {
		super();
		this.categoryID = categoryID;
		this.discontinued = discontinued;
		this.productName = productName;
		this.quantityPerUnit = quantityPerUnit;
		this.reorderLevel = reorderLevel;
		this.supplier = supplier;
		this.unitPrice = unitPrice;
		this.unitsInStock = unitsInStock;
		this.unitsOnOrder = unitsOnOrder;
	}
//	@ManyToOne(optional=false)
//	@JoinColumn(name="\"CategoryID\"")
//	private Category category;

//	public Category getCategory() {
//		return category;
//	}
//
//	public void setCategory(Category category) {
//		this.category = category;
//	}
}