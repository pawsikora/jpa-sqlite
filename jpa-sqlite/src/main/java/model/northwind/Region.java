package model.northwind;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Regions" database table.
 * 
 */
@Entity
@Table(name="\"Regions\"")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="\"RegionID\"")
	private int regionID;

	@Column(name="\"RegionDescription\"")
	private String regionDescription;

	public Region(String regionDescription) {
		super();
		this.regionDescription = regionDescription;
	}

	public Region() {
	}

	public String getRegionDescription() {
		return this.regionDescription;
	}

	public void setRegionDescription(String regionDescription) {
		this.regionDescription = regionDescription;
	}

	public int getRegionID() {
		return this.regionID;
	}

	public void setRegionID(int regionID) {
		this.regionID = regionID;
	}

}