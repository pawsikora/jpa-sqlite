package model.northwind;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Shippers" database table.
 * 
 */
@Entity
@Table(name="\"Shippers\"")
public class Shipper implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"ShipperID\"")
	private int shipperID;

	@Column(name="\"CompanyName\"")
	private String companyName;

	@Column(name="\"Phone\"")
	private String phone;
	
	public Shipper(int shipperID, String companyName, String phone) {
		super();
		this.shipperID = shipperID;
		this.companyName = companyName;
		this.phone = phone;
	}

	public Shipper() {
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getShipperID() {
		return this.shipperID;
	}

	public void setShipperID(int shipperID) {
		this.shipperID = shipperID;
	}

}