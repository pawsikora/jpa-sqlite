package model.northwind;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "Territories" database table.
 * 
 */
@Entity
@Table(name="\"Territories\"")
public class Territory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="\"TerritoryID\"")
	private String territoryID;

	@Column(name="\"RegionID\"")
	private int regionID;

	@Column(name="\"TerritoryDescription\"")
	private String territoryDescription;

	public Territory() {
	}

	public int getRegionID() {
		return this.regionID;
	}

	public void setRegionID(int regionID) {
		this.regionID = regionID;
	}

	public String getTerritoryDescription() {
		return this.territoryDescription;
	}

	public void setTerritoryDescription(String territoryDescription) {
		this.territoryDescription = territoryDescription;
	}

	public String getTerritoryID() {
		return this.territoryID;
	}

	public void setTerritoryID(String territoryID) {
		this.territoryID = territoryID;
	}

}