package pl.agh.edu.bazy.crud;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.parsers.ParserConfigurationException;

import org.sqlite.JDBC;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

import model.northwind.Category;
import model.northwind.Customer;
import model.northwind.Employee;
import model.northwind.Order;
import model.northwind.OrderDetail;
import model.northwind.Product;
import model.northwind.Shipper;
import model.northwind.Supplier;

public class DataLoader {

	public DataLoader() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws ParserConfigurationException
	 */
	
	
	public static void main(String[] args) throws ParserConfigurationException {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("jpa-sqlite");
		EntityManager entityManager = entityManagerFactory
				.createEntityManager();

		DataParser dataParser = new DataParser();
		
		// entityManager.flush();
		// entityManager.clear();
		System.out.println(entityManager.createNativeQuery("PRAGMA journal_mode").getSingleResult());
		
		entityManager.getTransaction().begin();
		for (Category category : dataParser.getCategories()) {
			entityManager.persist(category);
		}
		entityManager.getTransaction().commit();
		
		Map<String, Customer> customers = dataParser.getCustomers();
		entityManager.getTransaction().begin();
		for (Customer customer : customers.values()) {
			entityManager.persist(customer);
		}
		entityManager.getTransaction().commit();
		entityManager.getTransaction().begin();
		for (Employee employee : dataParser.getEmployees()) {
			entityManager.persist(employee);
		}
		
		List<Supplier> suppliers = dataParser.getSuppliers();
		entityManager.getTransaction().commit();
		entityManager.getTransaction().begin();
		for (Supplier supplier : suppliers) {
			entityManager.persist(supplier);
		}
		Map<Integer, Supplier> suppliersMap = new HashMap<Integer, Supplier>();
		for (Supplier supplier : suppliers) {
			suppliersMap.put(supplier.getSupplierID(), supplier);
		}
		
		Map<Integer, Shipper> shipperMap = dataParser.getShippers();
		entityManager.getTransaction().commit();
		entityManager.getTransaction().begin();
		for (Shipper shipper : shipperMap.values()) {
			entityManager.persist(shipper);
		}
		entityManager.getTransaction().commit();

		List<Product> products = dataParser.getProducts(suppliersMap);
		entityManager.getTransaction().begin();
		for (Product product : products) {
			entityManager.persist(product);
		}
		entityManager.getTransaction().commit();
		
		Map<Integer, Product> productsMap = new HashMap<Integer, Product>();
		for(Product product : products) {
			productsMap.put(product.getProductID(), product);
		}
		
		List<Order> orders = dataParser.getOrders(customers, shipperMap);
		Map<Integer, Order> ordersMap = new HashMap<Integer, Order>();
		for (Order order : orders) {
			ordersMap.put(order.getOrderID(), order);
		}
		List<OrderDetail> order_Details = dataParser.getOrderDetails(productsMap, ordersMap);
		
		System.out.println(orders.size());
		System.out.println(order_Details.size());

		
	
		
		entityManager.getTransaction().begin();
		int odIt = 0;
		for (int i = 0; i < 30; i++) {
			long start = System.currentTimeMillis();
			for (int j = 0; j < 1000; j++) {
				Order order = orders.get(i * 1000 + j);
				OrderDetail od = null;
				while (odIt < order_Details.size() && order_Details.get(odIt).getOrder().getOrderID() <= order.getOrderID()) {
					od = order_Details.get(odIt++);
					entityManager.persist(order_Details.get(odIt++));
					entityManager.createNamedQuery("insertOrderDetail").setParameter(1, od.getOdID()).setParameter(2, od.getOrder().getOrderID()).setParameter(3, od.getProductID()).setParameter(4, od.getUnitPrice()).setParameter(5, od.getQuantity()).setParameter(6, od.getDiscount()).executeUpdate();
				}
			}
			//entityManager.getTransaction().commit();
			System.out.println(i + ", " + (System.currentTimeMillis() - start));
		}
		//entityManager.getTransaction().begin();
		for (int i = 30 * 1000; i < orders.size(); ++i) {	
			Order order = orders.get(i);
			entityManager.persist(order);
			

			while (odIt < order_Details.size()
					&& order_Details.get(odIt).getOrder().getOrderID() <= order.getOrderID()) {
				entityManager.persist(order_Details.get(odIt++));
			}

		}
		entityManager.getTransaction().commit();
	}

}
