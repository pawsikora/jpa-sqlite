package pl.agh.edu.bazy.crud;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import model.northwind.Category;
import model.northwind.Customer;
import model.northwind.Employee;
import model.northwind.Order;
import model.northwind.OrderDetail;
import model.northwind.Product;
import model.northwind.Shipper;
import model.northwind.Supplier;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DataParser {

	private final String PATH = "embedded_database/sampledata/";
	private DocumentBuilder dBuilder;

	public DataParser() throws ParserConfigurationException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dBuilder = dbFactory.newDocumentBuilder();
	}

	public List<Category> getCategories() {
		Document doc;
		List<Category> categories = new ArrayList<Category>();
		try {
			doc = dBuilder.parse(new File(PATH + "categories.xml"));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("category");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					categories.add(new Category(element
							.getElementsByTagName("CategoryName").item(0)
							.getTextContent(), element
							.getElementsByTagName("Description").item(0)
							.getTextContent()));
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categories;

	}

	public Map<Integer, Shipper> getShippers() {
		Document doc;
		Map<Integer, Shipper> shippers = new HashMap<Integer, Shipper>();
		try {
			doc = dBuilder.parse(new File(PATH + "shippers.xml"));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("shipper");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					shippers.put(
							Integer.parseInt(element
									.getElementsByTagName("ShipperID").item(0)
									.getTextContent()),
							new Shipper(Integer.parseInt(element
									.getElementsByTagName("ShipperID").item(0)
									.getTextContent()), element
									.getElementsByTagName("CompanyName")
									.item(0).getTextContent(), element
									.getElementsByTagName("Phone").item(0)
									.getTextContent()));
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return shippers;

	}

	public Map<String, Customer> getCustomers() {
		Document doc;
		Map<String, Customer> customers = new HashMap<String, Customer>();
		try {
			doc = dBuilder.parse(new File(PATH + "customers.xml"));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("customer");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					customers.put(
							element.getElementsByTagName("CustomerID").item(0)
									.getTextContent(),
							new Customer(element
									.getElementsByTagName("CustomerID").item(0)
									.getTextContent(), element
									.getElementsByTagName("Address").item(0)
									.getTextContent(), element
									.getElementsByTagName("City").item(0)
									.getTextContent(), element
									.getElementsByTagName("CompanyName")
									.item(0).getTextContent(), element
									.getElementsByTagName("ContactName")
									.item(0).getTextContent(), element
									.getElementsByTagName("ContactTitle")
									.item(0).getTextContent(), element
									.getElementsByTagName("Country").item(0)
									.getTextContent(), element
									.getElementsByTagName("Phone").item(0)
									.getTextContent()));
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customers;

	}

	public List<Product> getProducts(Map<Integer, Supplier> suppliers) {
		Document doc;
		List<Product> products = new ArrayList<Product>();
		try {
			doc = dBuilder.parse(new File(PATH + "products.xml"));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("product");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					Product product = new Product(Integer.parseInt(element
							.getElementsByTagName("CategoryID").item(0)
							.getTextContent()), element
							.getElementsByTagName("Discontinued").item(0)
							.getTextContent(), element
							.getElementsByTagName("ProductName").item(0)
							.getTextContent(), element
							.getElementsByTagName("QuantityPerUnit").item(0)
							.getTextContent(), Integer.parseInt(element
							.getElementsByTagName("ReorderLevel").item(0)
							.getTextContent()), suppliers.get(Integer
							.parseInt(element
									.getElementsByTagName("SupplierID").item(0)
									.getTextContent())),
							Float.parseFloat(element
									.getElementsByTagName("UnitPrice").item(0)
									.getTextContent()),
							Integer.parseInt(element
									.getElementsByTagName("UnitsInStock")
									.item(0).getTextContent()),
							Integer.parseInt(element
									.getElementsByTagName("UnitsOnOrder")
									.item(0).getTextContent()));
					products.add(product);
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;

	}

	public List<Order> getOrders(Map<String, Customer> customers,
			Map<Integer, Shipper> shipperMap) {
		Document doc;
		List<Order> orders = new ArrayList<Order>();
		NodeList nList;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			doc = dBuilder.parse(new File(PATH + "orders.xml"));
			doc.getDocumentElement().normalize();
			nList = doc.getElementsByTagName("order");

			String filePaths[] = { "orders_rand_10000.xml",
					"orders_rand_20000.xml" };
			for (String filePath : filePaths) {
				doc = dBuilder.parse(new File(PATH + filePath));
				doc.getDocumentElement().normalize();
				nList = doc.getElementsByTagName("order");
				for (int i = 0; i < nList.getLength(); i++) {
					Node node = nList.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						try {
							orders.add(new Order(Integer.parseInt(element
									.getElementsByTagName("OrderID").item(0)
									.getTextContent()), customers.get(element
									.getElementsByTagName("CustomerID").item(0)
									.getTextContent()), Integer
									.parseInt(element
											.getElementsByTagName("EmployeeID")
											.item(0).getTextContent()), Float
									.parseFloat(element
											.getElementsByTagName("Freight")
											.item(0).getTextContent()), df
									.parse(element
											.getElementsByTagName("OrderDate")
											.item(0).getTextContent()), df
									.parse(element
											.getElementsByTagName(
													"RequiredDate").item(0)
											.getTextContent()), element
									.getElementsByTagName("ShipAddress")
									.item(0).getTextContent(), element
									.getElementsByTagName("ShipCity").item(0)
									.getTextContent(), element
									.getElementsByTagName("ShipCountry")
									.item(0).getTextContent(), element
									.getElementsByTagName("ShipName").item(0)
									.getTextContent(), df.parse(element
									.getElementsByTagName("ShippedDate")
									.item(0).getTextContent()), shipperMap
									.get(Integer.parseInt(element
											.getElementsByTagName("ShipVia")
											.item(0).getTextContent()))));
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (DOMException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orders;

	}

	public List<OrderDetail> getOrderDetails(Map<Integer, Product> products,
			Map<Integer, Order> orders) {
		Document doc;
		List<OrderDetail> order_Details = new ArrayList<OrderDetail>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		NodeList nList;
		try {
			String filenames[] = { "orderdetails_rand_10000.xml",
					"orderdetails_rand_20000.xml" };
			for (String filename : filenames) {
				doc = dBuilder.parse(new File(PATH + filename));
				doc.getDocumentElement().normalize();
				nList = doc.getElementsByTagName("orderdetail");
				for (int i = 0; i < nList.getLength(); i++) {
					Node node = nList.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						try {
							order_Details
									.add(new OrderDetail(
											orders.get(Integer.parseInt(element
													.getElementsByTagName(
															"OrderID").item(0)
													.getTextContent())),
											products.get(Integer
													.parseInt(element
															.getElementsByTagName(
																	"ProductID")
															.item(0)
															.getTextContent())),
											Float.parseFloat(element
													.getElementsByTagName(
															"Discount").item(0)
													.getTextContent()),
											Integer.parseInt(element
													.getElementsByTagName(
															"Quantity").item(0)
													.getTextContent()),
											Float.parseFloat(element
													.getElementsByTagName(
															"UnitPrice")
													.item(0).getTextContent())));
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (DOMException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}

		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return order_Details;

	}

	public List<Employee> getEmployees() {
		Document doc;
		List<Employee> employees = new ArrayList<Employee>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			doc = dBuilder.parse(new File(PATH + "employees.xml"));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("employee");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					try {
						employees.add(new Employee(element
								.getElementsByTagName("Address").item(0)
								.getTextContent(), new Date(df.parse(
								element.getElementsByTagName("BirthDate")
										.item(0).getTextContent()).getTime()),
								element.getElementsByTagName("City").item(0)
										.getTextContent(), element
										.getElementsByTagName("Country")
										.item(0).getTextContent(), element
										.getElementsByTagName("Extension")
										.item(0).getTextContent(), element
										.getElementsByTagName("FirstName")
										.item(0).getTextContent(), new Date(df
										.parse(element
												.getElementsByTagName(
														"HireDate").item(0)
												.getTextContent()).getTime()),
								element.getElementsByTagName("HomePhone")
										.item(0).getTextContent(), element
										.getElementsByTagName("LastName")
										.item(0).getTextContent(), element
										.getElementsByTagName("Notes").item(0)
										.getTextContent(), element
										.getElementsByTagName("PostalCode")
										.item(0).getTextContent(), Integer
										.parseInt(element
												.getElementsByTagName(
														"ReportsTo").item(0)
												.getTextContent()), element
										.getElementsByTagName("Title").item(0)
										.getTextContent(),
								element.getElementsByTagName("TitleOfCourtesy")
										.item(0).getTextContent()));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DOMException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return employees;

	}

	public List<Supplier> getSuppliers() {
		Document doc;
		List<Supplier> suppiers = new ArrayList<Supplier>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			doc = dBuilder.parse(new File(PATH + "suppliers.xml"));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("supplier");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					try {
						suppiers.add(new Supplier(element
								.getElementsByTagName("Address").item(0)
								.getTextContent(), element
								.getElementsByTagName("City").item(0)
								.getTextContent(), element
								.getElementsByTagName("CompanyName").item(0)
								.getTextContent(), element
								.getElementsByTagName("ContactName").item(0)
								.getTextContent(), element
								.getElementsByTagName("ContactTitle").item(0)
								.getTextContent(), element
								.getElementsByTagName("Country").item(0)
								.getTextContent(), element
								.getElementsByTagName("Phone").item(0)
								.getTextContent(), element
								.getElementsByTagName("PostalCode").item(0)
								.getTextContent()));
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (DOMException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return suppiers;

	}

}
