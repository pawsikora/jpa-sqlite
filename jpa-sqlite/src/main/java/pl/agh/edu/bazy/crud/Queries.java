package pl.agh.edu.bazy.crud;

import java.text.DecimalFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Queries {
	private static EntityManager entityManager;

	// ile zam�wie� z ka�dego kraju zosta�o zrealizowanych
	private static void query1() {
		Query query = entityManager
				.createQuery("SELECT c.country, count(*) FROM Order o JOIN o.customer c GROUP BY c.country");
		Long start = System.currentTimeMillis();
		List<Object[]> list = query.getResultList();
		int sum = 0;
		for (Object[] object : list) {
			System.out.print(object[0] + ": " + object[1] + ", ");
		}
		System.out.println(System.currentTimeMillis() - start);
	}

	// jaki by� �redni czas realizacji zam�wienia w ka�dym roku
	private static void query2() {
		Query query = entityManager
				.createQuery("SELECT YEAR(o.orderDate), AVG(DATEDIFF(o.shippedDate, o.orderDate)) FROM Order o GROUP BY YEAR(o.orderDate)");
		Long start = System.currentTimeMillis();
		List<Object[]> list = query.getResultList();
//		for (Object[] object : list) {
//			System.out.print(object[0] + ": " + new DecimalFormat("#.##").format((Double)object[1]/3600/24) + ", ");
//		}
		System.out.println(System.currentTimeMillis() - start);

	}

	// ile sztuk produkt�w od ka�dego z dostawc�w uda�o si� sprzeda�
	private static void query3() {
		Query query = entityManager
				.createQuery("SELECT s.supplierID, SUM(od.quantity) FROM OrderDetail od JOIN od.product p JOIN p.supplier as s GROUP BY s.supplierID");
		Long start = System.currentTimeMillis();
		List<Object[]> list = query.getResultList();
//		for (Object[] object : list) {
//			System.out.print(object[0] + ": " + object[1] + ", ");
//		}
		System.out.println(System.currentTimeMillis() - start);
	}

	// jaka kwota zam�wie� by�a zg�aszana w ka�dy z dni tygodnia
	private static void query4() {
		Query query = entityManager
				.createQuery("SELECT DAYOFWEEK(o.orderDate), SUM((1 - od.discount) * od.quantity * od.unitPrice) FROM OrderDetail od JOIN od.order o GROUP BY DAYOFWEEK(o.orderDate)");
		Long start = System.currentTimeMillis();
		List<Object[]> list = query.getResultList();
//		for (Object[] object : list) {
//			System.out.println(object[0] + ": " + new DecimalFormat("#.##").format(object[1]));
//		}
		System.out.println(System.currentTimeMillis() - start);
	}

	// jaka by�a warto�� produkt�w zam�wionych z ka�dego z kraj�w w ka�dym roku;
	// chodzi o kraj zamawiaj�cego
	private static void query5() {
		Query query = entityManager
				.createQuery("SELECT c.country, YEAR(o.orderDate), SUM((1 - od.discount) * od.quantity * od.unitPrice) FROM OrderDetail od JOIN od.order o JOIN o.customer c GROUP BY c.country, YEAR(o.orderDate)");
		Long start = System.currentTimeMillis();
		List<Object[]> list = query.getResultList();
//		for (Object[] object : list) {
//			System.out.println(object[0] + ": " + object[1] + ": " + new DecimalFormat("#.##").format(object[2]));
//		}
		System.out.println(System.currentTimeMillis() - start);
	}

	// jaka by�a �rednia warto�� jednej sztuki produktu dla ka�dego ze
	// spedytor�w w ka�dym roku
	private static void query6() {
		Query query = entityManager
				.createQuery("SELECT sv.shipperID, YEAR(o.orderDate), SUM(od.unitPrice * od.quantity) / SUM(od.quantity) FROM OrderDetail od JOIN od.order o JOIN o.shipVia sv GROUP BY sv.shipperID, YEAR(o.orderDate)");
		Long start = System.currentTimeMillis();
		List<Object[]> list = query.getResultList();
//		for (Object[] object : list) {
//			System.out.println(object[0] + ": " + object[1] + ": " + new DecimalFormat("#.##").format(object[2]));
//		}
		System.out.println(System.currentTimeMillis() - start);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence
				.createEntityManagerFactory("jpa-sqlite");
		entityManager = entityManagerFactory.createEntityManager();

		query1();
		query2();
		query3();
		query4();
		query5();
		query6();
		
	}

}
