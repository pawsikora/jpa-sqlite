package pl.agh.edu.bazy.driver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.sqlite.JDBC;

public class AdikiDriver extends JDBC {

	static {
		try {
			Class.forName("org.sqlite.JDBC");
            final Driver driver = DriverManager.getDriver(JDBC.PREFIX);
            DriverManager.deregisterDriver(driver);
            DriverManager.registerDriver(new AdikiDriver());
        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Connection connect(String url, Properties info) throws SQLException {
		Connection connection = super.connect(url, info);
		initPragmas(connection);
		return connection; 
	}

	private void initPragmas(Connection connection) throws SQLException {
		//connection.prepareStatement("PRAGMA temp_store = 2;").execute();
		
	}

}
